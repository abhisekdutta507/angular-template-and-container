import { Component } from '@angular/core';

interface Map {
  label: string;
  value: string;
  hide?: boolean;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  commands: Map[] = [
    {
      label: 'New Component',
      value: 'component'
    },
    {
      label: 'Add Custom Directive',
      value: 'directive'
    },
    {
      label: 'Angular Material',
      value: 'material',
      hide: true
    },
    {
      label: 'Add PWA Support',
      value: 'pwa'
    },
    {
      label: 'Add Dependency',
      value: 'dependency'
    },
    {
      label: 'Run and Watch Tests',
      value: 'test'
    },
    {
      label: 'Build for Production',
      value: 'build'
    }
  ];

  selection = {
    value: ''
  };

  myContext = {
    $implicit: 'Abhisek Dutta',
    facebook: 'https://www.facebook.com/abhisek.dutta.507',
    linkedIn: 'https://www.linkedin.com/in/abhisek-dutta-944273184/',
    bitbucket: 'https://bitbucket.org/abhisekdutta507/'
  };
}
