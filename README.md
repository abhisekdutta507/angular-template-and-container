This project is created with [Angular CLI](https://angular.io/cli).
We have typescript support in this project using [typescript](https://code.visualstudio.com/docs/typescript/typescript-compiling) with [webpack-cli](https://webpack.js.org/guides/installation/).

Press  `Cmd + Shift + V`  form your Mac keyboard to preview the README.md.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode. Then, visit http://localhost:4200 through a browser window to run the application.

## Learn More

You can learn more in the [Angular Directives](https://angular.io/guide/structural-directives).

### ng-template usage in Angular projects

**Initialize the project with `NPM`.** *Make sure you have a stable version of* [NodeJS](https://nodejs.org/en/), [NPM](https://www.npmjs.com/) & [Angular CLI](https://angular.io/cli) *is installed in your system globally.*

```bash
ng new angularTemplate
```

Answer some questions asked by the `Angular CLI` when initiating the new project.

**Check** the `app.component.html` file to understand the directives. Visit [app.component.html](https://bitbucket.org/abhisekdutta507/angular-template-and-container/src/master/src/app/app.component.html).

`ng-container` directive. Visit [ng-container](https://angular.io/api/common/NgComponentOutlet) for more info.

```html
<!--
    1. ng-container is used as a non-rendered container to avoid having to add a span or a div. Similar to React Fragment.
-->
<ng-container *ngFor="let item of commands; index as i">
    <ng-container *ngIf="item?.hide">
        ...
    </ng-container>
</ng-container>
```

`ng-template` directive. Visit [ng-template](https://angular.io/guide/structural-directives) for more info.

```html
<!-- 
    1. NgTemplate is a representation of structural directives
    2. NgForOf directive is used with NgTemplate
    3. NgTemplate allows us to group some content that is not rendered directly but can be used in other places of our HTML template.
-->
<ng-template ngFor [ngForOf]="commands" let-i="index" let-item>
    <!-- 
    1. NgIf directive is used with NgTemplate
    -->
    <ng-template [ngIf]="!item?.hide">
        ...
    </ng-template>
</ng-template>
```

`*ngSwitchCase` directive. Visit [*ngSwitchCase](https://angular.io/api/common/NgSwitch) for more info.

```html
<!--
    1. NgSwitchCase and NgSwitchDefault directives are used with NgTemplate
-->
<div class="terminal" [ngSwitch]="selection.value">
  <ng-template [ngSwitchDefault]>
    <pre>ng generate component xyz</pre>
  </ng-template>
  <ng-template [ngSwitchCase]="'directive'">
    <pre>ng g directive def</pre>
  </ng-template>
  ...
</div>
```

`*ngTemplateOutlet` directive. Visit [ngTemplateOutlet](https://angular.io/api/common/NgComponentOutlet) for more info.

```html
<!-- 
  1. NgTemplateOutlet directive is used to render NgTemplate
-->
<ng-container *ngTemplateOutlet="profile; context: myContext"></ng-container>
```

`Template microsyntax` is used to configure directive in a compact, friendly string. Visit [Template microsyntax](https://angular.io/guide/structural-directives#microsyntax) for more info.

```html
<ng-template #profile let-name let-fb="facebook" let-li="linkedIn" let-git="bitbucket">
    ...
</ng-template>
```

**TypeScript** file example.

```ts
myContext = {
  $implicit: 'Abhisek Dutta',
  facebook: 'https://www.facebook.com/abhisek.dutta.507',
  linkedIn: 'https://www.linkedin.com/in/abhisek-dutta-944273184/',
  bitbucket: 'https://bitbucket.org/abhisekdutta507/'
};
```

Hurray!! we have learned the basic concepts of Angular template & container.